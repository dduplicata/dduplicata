import * as checksum from "checksum";

export interface LoadedChecksum {
    checksum: string;
    fileName: string;
}

export interface RecCallParams {
    path: string;
    filter?: RegExp[];
}

export interface FindChecksum {
    params: RecCallParams[];
    results: LoadedChecksum;
}

export interface FindOptions {
    options: checksum.ChecksumOptions;
    params: RecCallParams;
    customLogger?: ILogger;
}

export interface ILogger {
    d(...args): void;

    i(...args): void;

    w(...args): void;

    e(...args): void;
}
