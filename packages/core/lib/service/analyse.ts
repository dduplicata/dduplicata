import * as checksum from 'checksum';
import {Observable} from "rxjs";
import type {LoadedChecksum} from "../types";

export class Analyse {

    constructor(
        private readonly defaultOptions: checksum.ChecksumOptions = {algorithm: 'md5'},
    ) {
    }

    public doAnalyse(fileName: string, option: checksum.ChecksumOptions = this.defaultOptions): Observable<LoadedChecksum> {
        return new Observable<LoadedChecksum>(subscriber => checksum.file(fileName, option, (error, hash) => {
            if (error) {
                subscriber.error(error);
            }
            subscriber.next({checksum: hash, fileName});
        }));
    }
}
