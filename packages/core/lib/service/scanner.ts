import {Analyse} from "./analyse";
import {Logger} from "./logger";
import * as fs from "fs";
import * as path from "path";
import {defer, EMPTY, expand, filter, map, merge, Observable, of, tap} from "rxjs";
import type {FindChecksum, RecCallParams} from "../types";
import {LoadedChecksum} from "../types";


export class Scanner {

    constructor(
        private readonly analyse: Analyse,
        private readonly logger: Logger
    ) {
    }

    public doScan(params: RecCallParams): Observable<LoadedChecksum> {
        return defer(() => of(params))
            .pipe(
                expand(value => this.listFiles(value.path, value.filter)),
                map(value => value.results),
                filter(i => !!i),
                tap(({checksum, fileName}) => this.logger.d('Scanned [%s] %s', checksum, fileName)),
            );
    }

    private listFiles(startPath: string, filter?: RegExp[]): Observable<FindChecksum> {
        if (!fs.existsSync(startPath)) {
            // leaf
            return EMPTY;
        }

        const obs$: Observable<FindChecksum>[] = [];

        const files: string[] = fs.readdirSync(startPath);
        for (const file of files) {
            const filename = path.join(startPath, file);
            const stat = fs.lstatSync(filename);

            if (stat.isDirectory()) {
                const recCall$ = this.listFiles(filename, filter);
                obs$.push(recCall$);
            } else if (!filter || filter.some(re => re.test(filename))) {
                // leaf
                const analyze$: Observable<FindChecksum> = this.analyse.doAnalyse(filename)
                    .pipe(map((value) => ({params: [], results: value})));
                obs$.push(analyze$);
            }
        }

        return merge(...obs$);
    }
}
