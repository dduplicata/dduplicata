import type {ILogger} from "../types";

export class Logger implements ILogger {
    d(...args) {
        console.debug(...args);
    }

    i(...args) {
        console.info(...args);
    }

    w(...args) {
        console.warn(...args);
    }

    e(...args) {
        console.error(...args);
    }
}
