import {findDuplicate} from './main';

export * from './types';
export * from './service';
export * from './main';

export default findDuplicate;
