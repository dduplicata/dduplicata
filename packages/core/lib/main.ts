import type {FindOptions, LoadedChecksum} from "./types";
import {Observable} from "rxjs";
import {Analyse, Logger, Scanner} from "./service";

export function findDuplicate({params, options, customLogger}: FindOptions): Observable<LoadedChecksum> {
    const logger = customLogger ?? new Logger();
    const analyze = new Analyse(options);
    const scanner = new Scanner(analyze, logger);
    return scanner.doScan(params);
}
