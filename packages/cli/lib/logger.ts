import winston, {format} from "winston";
import type {LoggerOptions} from "./types";
import type {ILogger} from "@dduplicata/core";

const appName = 'duda';

export function createLogger(options: LoggerOptions): ILogger {
    const logger = winston.createLogger({
        level: options.logLevel,
        format: format.combine(
            format.splat(),
            format.label({label: appName}),
            format.timestamp(),
            format.cli(),
        ),
        defaultMeta: {
            service: appName,
        },
        transports: [
            new winston.transports.Console(),
        ],
    });

    return {
        d: (m, ...args) => {
            logger.debug(m, ...args);
        },
        i: (m, ...args) => {
            logger.info(m, ...args);
        },
        w: (m, ...args) => {
            logger.warn(m, ...args);
        },
        e: (m, ...args) => {
            logger.error(m, ...args);
        },
    };
}
