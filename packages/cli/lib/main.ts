import yargs from "yargs";
import {hideBin} from "yargs/helpers";
import {createLogger} from "./logger";
import {check} from "./check";

export async function main() {
    return yargs(hideBin(process.argv))
        .scriptName('duda')
        .usage('$0 <cmd> [args]')
        .options({
            logLevel: {
                alias: 'l',
                describe: 'Define the log level',
                default: 'info',
                choices: ['info', 'debug', 'error', 'warn']
            },
        })
        .command(
            'check <folderUrl>',
            'Check a folder for duplicate files',
            () => {
                return yargs
                    .positional('folderUrl', {
                        type: 'string',
                        describe: 'The folder to analyse'
                    })
                    .option('pattern', {
                        type: 'string',
                        describe: 'The pattern of files to check',
                        alias: 'p'
                    })
                    .option('encoding', {
                        type: 'string',
                        describe: 'The encoding to use for checksum',
                        alias: 'e'
                    })
                    .option('algorithm', {
                        type: 'string',
                        describe: 'The algorithm to use for checksum',
                        alias: 'a'
                    });
            },
            (argv: any) => {
                const logger = createLogger(argv);
                check(argv, logger);
            }
        )
        .help()
        .demandCommand(1)
        .parse();
}
