export interface CheckOptions {
    folderUrl?: string;
    pattern?: string;
    algorithm?: string;
    encoding?: string;
}


export interface LoggerOptions {
    logLevel?: string;
}
