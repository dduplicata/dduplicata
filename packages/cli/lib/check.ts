import findDuplicate, {ILogger} from "@dduplicata/core";
import type {CheckOptions} from "./types";

export function check(argv: CheckOptions, logger: ILogger): void {
    logger.i('Checking files in ' + argv.folderUrl);
    const loaded: Record<string, string[]> = {};

    findDuplicate({
        params: {
            path: argv.folderUrl!,
            filter: argv.pattern ? [new RegExp(argv.pattern)] : undefined
        },
        options: {
            algorithm: argv.algorithm,
            encoding: argv.encoding,
        },
        customLogger: logger,
    }).subscribe((value) => {
        const duplicate = value.checksum in loaded;
        if (!duplicate) {
            loaded[value.checksum] = [];
        }

        loaded[value.checksum].push(value.fileName);

        if (duplicate) {
            logger.i('Found duplicates %s: %s', value.checksum, loaded[value.checksum]);
        }
    });
}


